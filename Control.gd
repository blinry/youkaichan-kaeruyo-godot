extends Control

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
    pass # Replace with function body.

func _input(event):
    if event.is_action_pressed("pause"):
        var new_state = not get_tree().paused
        get_tree().paused = new_state
        visible = new_state