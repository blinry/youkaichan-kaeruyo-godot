extends Control

func _ready():
    $NewGame.grab_focus()
    Game.get_node("TitleMusic").play()

func _on_NewGame_pressed():
    get_tree().change_scene("menu/Story1.tscn")

func _on_Quit_pressed():
    get_tree().quit()
