extends Control

func _ready():
    pass

func _input(event):
    if event.is_action_pressed("ui_accept"):
        Game.get_node("TitleMusic").stop()
        Game.get_node("GameMusic").play()
        get_tree().change_scene("levels/Level.tscn")