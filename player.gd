extends KinematicBody2D

export var active = true

var vel = Vector2()
var UP = Vector2(0, -1)
var move_speed = 8*15*8

var gravity
var max_jump_velocity
var min_jump_velocity
var max_jump_height = 4.5 * Game.TILESIZE
var min_jump_height = 1 * Game.TILESIZE
var jump_duration = 0.3

var on_floor

func _ready():
    gravity = 2*max_jump_height / pow(jump_duration, 2)
    max_jump_velocity = -sqrt(2*gravity*max_jump_height)
    min_jump_velocity = -sqrt(2*gravity*min_jump_height)
    $AnimationPlayer.playback_speed = rand_range(0.8, 1.2)
    $AnimationPlayer.seek(rand_range(0, 1.6))
    
func _input(event):
    if event.is_action_pressed("btn_up") and on_floor:
        vel.y = max_jump_velocity
        on_floor = false
        $Jump.pitch_scale = rand_range(1.0, 1.4)
        get_node("Jump").play()
    if event.is_action_released("btn_up") && vel.y < -100:
        vel.y = -100

func _process(delta):
    _get_input()
    vel.y += gravity*delta
    vel = move_and_slide(vel, UP)
    
    
    if is_on_floor():
        on_floor = true
        
    if on_floor:
        $Body.scale.y = 1
    else:
        $Body.scale.y = 1.5
    
func _get_input():
    var move_direction = Input.get_action_strength("btn_right") - Input.get_action_strength("btn_left")
    vel.x = lerp(vel.x, move_speed * move_direction, _get_horizontal_w())
    if move_direction != 0:
        $AnimationPlayer.play("walk")
        $Body.scale.x = -sign(move_direction)
        #$AnimationPlayer.playback_speed = abs(move_direction)*1.5
    else:
        $AnimationPlayer.play("idle")
        
func _get_horizontal_w():
    return 0.1

func footstep():
    if not is_on_floor():
        return
    #$Jump.play()
    var dust = preload("res://characters/effects/Dust.tscn").instance()
    dust.position = position
    get_parent().add_child(dust)